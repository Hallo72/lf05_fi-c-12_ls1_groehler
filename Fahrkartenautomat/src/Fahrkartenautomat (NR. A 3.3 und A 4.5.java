
	import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       double zuZahlenderBetrag; 
       double rückgabebetrag;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();     
       // Geldeinwurf
       // -----------
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(rückgabebetrag);
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
    public static double fahrkartenbestellungErfassen() {
    	Scanner scn = new Scanner(System.in);
    	double zuZahlenderBetrag;
    	double anzahlTickets;
    	
    	System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = scn.nextDouble();
        
        System.out.print("Wie viele Tickets wollen Sie?");
        anzahlTickets = scn.nextInt();
        zuZahlenderBetrag *= anzahlTickets;
        return zuZahlenderBetrag;
    	
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner scn2 = new Scanner(System.in);
    	double rückgabeBetrag;
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	
    	
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f €\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = scn2.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	rückgabeBetrag = eingezahlterGesamtbetrag - zuZahlen;
    	
    	return rückgabeBetrag;
    	
    }

    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");    	
    }

    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    }
}



class FahrkartenautomatEndlos{
    public static void main(String[] args){
       double zuZahlenderBetrag; 
       double rückgabebetrag;
       while(true) {
    	   //Start der Bestell erfassung
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();     
    	   // Geldeinwurf
    	   // -----------
    	   rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	   // Fahrscheinausgabe
    	   // -----------------
    	   fahrkartenAusgeben();
    	   // Rückgeldberechnung und -Ausgabe
    	   // -------------------------------
    	   rueckgeldAusgeben(rückgabebetrag);
    	   System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    	   System.out.println(" ");
    	   System.out.println(" ");
           }
    }
        
    public static double fahrkartenbestellungErfassen() {
    	Scanner scn = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	double anzahlTickets;
    	int wahl;
    	// Bestell vorgang
    	System.out.println("Fahrkartenbestellvorgang:");
    	System.out.println("=========================");
    	System.out.println("Wählen sie Ihr Wunschticket für Berlin AB aus: ");
    	System.out.println("	 Regeltarif AB [2,90 EUR]			(1)");
    	System.out.println("Tageskarte Regeltarif AB [8,60 EUR] 				(2)");
    	System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]	 	(3)");
    	System.out.print("Ihre Wahl: ");
    	wahl=scn.nextInt();
    	//kontrolle der Auswahl
    	 while(wahl < 1 || wahl > 3) {
    		 System.out.println("falsche Eingabe");
    		 System.out.print("Ihre Wahl: ");
    		 wahl=scn.nextInt();    		 
    	 }
    	 // Nach der Wahl den zu zahlenden Betrag festlegen (pro ticket)
    	if (wahl == 1) {
    		zuZahlenderBetrag= 2.9;
    	}
    	if (wahl == 2) {
    		zuZahlenderBetrag= 8.6;
    	}
    	if (wahl == 3) {
    		zuZahlenderBetrag= 23.50;
    	}
        
        System.out.print("Wie viele Tickets wollen Sie? (maximal 10)");
        anzahlTickets = scn.nextInt();
        
        //kontrolle anzahlTickets
        while(anzahlTickets < 0 || anzahlTickets > 10) {
        	System.out.println("Falsche Anzahl. Bitte erneut eingeben");
        	anzahlTickets = scn.nextInt();
        }
        zuZahlenderBetrag *= anzahlTickets;
        return zuZahlenderBetrag;
    	
    }
   
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner scn2 = new Scanner(System.in);
    	double rückgabeBetrag;
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f €\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = scn2.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	rückgabeBetrag = eingezahlterGesamtbetrag - zuZahlen;
    	
    	return rückgabeBetrag;
    	
    }

    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");    	
    }
    
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
    }
}

	