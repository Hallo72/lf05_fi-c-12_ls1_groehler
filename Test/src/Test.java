

import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	public Raumschiff() {
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildInProzent,
			int huelleInProzent, int lebenserhaltungssystemInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildInProzent = schildInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildInProzent() {
		return schildInProzent;
	}

	public void setSchildInProzent(int schildInProzent) {
		this.schildInProzent = schildInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemInProzent() {
		return lebenserhaltungssystemInProzent;
	}

	public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemInProzent) {
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	public void photonentorpedoSchiessen() {
		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			photonentorpedoAnzahl -= 1;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(null);
		}
	}

	public void phaserkanoneSchiessen() {
		if (energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			energieversorgungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
		}
	}

	private void treffer(Raumschiff r) {
		int zwischenSpeicher;
		System.out.println(r.getSchiffsname() + " wurde getroffen");
		zwischenSpeicher = r.getSchildInProzent() - 50;
		// ueberpruefung ob schild ob schild zerst rt
		if (zwischenSpeicher <= 0) {
			r.setSchildInProzent(0);
			zwischenSpeicher = r.getEnergieversorgungInProzent() - 50;
			// ueberpruefung ob Energieversorgung unter 0 ist wenn das ist wird auf null
			// gesetz da negative Werte nicht schoen bzw. sinnvoll sind
			if (zwischenSpeicher < 0) {
				r.setEnergieversorgungInProzent(0);
			} else {
				r.setEnergieversorgungInProzent(zwischenSpeicher);
			}
			zwischenSpeicher = r.getHuelleInProzent() - 50;
			// Pruefung ob Huelle weniger 0 falls der fall wird auf 0 gesetz um negative
			// zahlen zu vermeiden
			if (zwischenSpeicher < 0) {
				r.setHuelleInProzent(0);
				r.setLebenserhaltungssystemInProzent(0);
				r.nachrichtAnAlle("Lebenserhaltungssytem wurde vernichtet");
			} else {
				r.setHuelleInProzent(zwischenSpeicher);
			}
		}
	}

	public void nachrichtAnAlle(String message) {
		System.out.println(message);
		broadcastKommunikator.add(message);
	}

	public ArrayList<String> eintraegeLogbuchZurueckGeben() {
		return broadcastKommunikator;
	}

	public void photonentorpedosLaden(int anzahlTorpedos) {
		String speicher;
		int menge = 0;
		int torpedos;
		int zahl = 0;
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			speicher = ladungsverzeichnis.get(i).getBezeichnung();
			if (speicher == "Photonentorpedo") {
				menge = ladungsverzeichnis.get(i).getMenge();
				// Speichern der Position der Torpedos im Ladungsverzeichnis
				zahl = i;
			}
			speicher = null;
		}
		if (menge == 0) {
			System.out.println("Keine Photonentorpedo Gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		} else {
			torpedos = menge - anzahlTorpedos;
			// Testen ob alle Torpedos geladen werden sollen
			if (torpedos < 0) {
				torpedos = getPhotonentorpedoAnzahl();
				setPhotonentorpedoAnzahl(torpedos + menge);
				ladungsverzeichnis.get(zahl).setMenge(0);
			} else {
				torpedos = getPhotonentorpedoAnzahl();
				setPhotonentorpedoAnzahl(torpedos + anzahlTorpedos);
				ladungsverzeichnis.get(zahl).setMenge(menge - anzahlTorpedos);
			}
		}
	}

	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {
		Random zufall = new Random();
		int counter = 0;
		int ergebnis = 0;
		int zahl = zufall.nextInt(100);
		// Zaehler f r Berechnung der reperatur
		if (schutzschilde == true) {
			counter += 1;
		}
		if (energieversorgung == true) {
			counter += 1;
		}
		if (schiffshuelle == true) {
			counter += 1;
		}
		// Ueberpruefung das nicht mehr Androiden genutzt werden als ueberhaupt da sind
		if (anzahlDroiden > getAndroidenAnzahl()) {
			ergebnis = zahl * getAndroidenAnzahl() / counter;
		}
		if (anzahlDroiden <= getAndroidenAnzahl()) {
			ergebnis = zahl * anzahlDroiden / counter;
		}
		// Reperatur inklusive test ob die Systeme  ber 100% liegen
		if (schutzschilde == true) {
			setSchildInProzent(getSchildInProzent() + ergebnis);
			if (getSchildInProzent() > 100) {
				setSchildInProzent(100);
			}
		}
		if (energieversorgung == true) {
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() + ergebnis);
			if (getEnergieversorgungInProzent() > 100) {
				setEnergieversorgungInProzent(100);
			}
		}
		if (schiffshuelle == true) {
			setHuelleInProzent(getHuelleInProzent() + ergebnis);
			if (getHuelleInProzent() > 100) {
				setHuelleInProzent(100);
			}
		}
	}

	public void zustandRaumschiff() {
		System.out.println("Schild:                 " + schildInProzent + "%");
		System.out.println("Huelle:                  " + huelleInProzent + "%");
		System.out.println("Lebenserhaltungssystem: " + lebenserhaltungssystemInProzent + "%");
		System.out.println("Energie:                " + energieversorgungInProzent + "%");
		System.out.println("Androiden Anzahl:       " + androidenAnzahl);
		System.out.println("Photonentorpedo Anzahl: " + photonentorpedoAnzahl + " St ck");
	}

	public void ladungsverzeichnisAusgeben() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out
					.println(ladungsverzeichnis.get(i).getBezeichnung() + ": " + ladungsverzeichnis.get(i).getMenge());
		}
	}

	public void ladungsverzeichnisAufräumen() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			if (ladungsverzeichnis.get(i).getMenge() == 0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}

	@Override
	public String toString() {
		return "Raumschiff [photonentorpedoAnzahl=" + photonentorpedoAnzahl + ", energieversorgungInProzent="
				+ energieversorgungInProzent + ", schildInProzent=" + schildInProzent + ", huelleInProzent="
				+ huelleInProzent + ", lebenserhaltungssystemInProzent=" + lebenserhaltungssystemInProzent
				+ ", androidenAnzahl=" + androidenAnzahl + ", schiffsname=" + schiffsname + ", broadcastKommunikator="
				+ broadcastKommunikator + ", ladungsverzeichnis=" + ladungsverzeichnis + "]";
	}
}




public class Ladung {
	private String bezeichnung;
	private int menge;

	public Ladung() {

	}

	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}
}

